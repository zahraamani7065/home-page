import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:home_page_project/Data/data.dart';
import 'package:home_page_project/Screens/PlayScreen.dart';
import 'package:home_page_project/Screens/SearchScreen.dart';
import 'package:home_page_project/Screens/callScreen.dart';
import 'package:home_page_project/Screens/profileScreen.dart';
import 'Widgets/BestArtists.dart';
import 'Widgets/BottomNavigation.dart';
import 'Widgets/Category.dart';
import 'Widgets/CategoryPart.dart';
import 'Widgets/EthnicPlaylists.dart';
import 'Widgets/LocationPlayList.dart';
import 'Widgets/NewestMusic.dart';
import 'Widgets/TopPlayList.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          textTheme: const TextTheme(
            subtitle2: TextStyle(fontSize: 16, color: Colors.white),
            headline6: TextStyle(
                fontSize: 20, fontWeight: FontWeight.bold, color: Colors.black),
            subtitle1: TextStyle(
                fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white),
            headline5: TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.bold,
                color: Colors.orangeAccent),
          ),
          primarySwatch: Colors.blue,
        ),
        home: MainScreen());
  }
}

class MainScreen extends StatefulWidget {
  MainScreen({super.key});

  @override
  State<MainScreen> createState() => _MainScreenState();
}

const int homeIndex = 0;
const int playIndex = 1;
const int searchIndex = 2;
const int callIndex = 3;
const int profileIndex = 4;

class _MainScreenState extends State<MainScreen> {
  int selectedScreenIndex = homeIndex;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        bottomNavigationBar: bottomNavigation(
          onTap: (int index) {
            setState(() {
              selectedScreenIndex = index;
            });
          },
          selectedIndex: selectedScreenIndex,
        ),
        body: IndexedStack(
          index: selectedScreenIndex,
          children: const [
            HomePage(),
            playScreen(),
            searchScreen(),
            callScreen(),
            profileScreen(),
          ],
        ));
  }
}

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: FutureBuilder<AutoGenerate>(
            future: getData(),
            builder: (context, snapshot) {
              if (snapshot.hasData && snapshot.data != null) {
                return SafeArea(
                    child: SingleChildScrollView(
                  physics: const BouncingScrollPhysics(),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      CategoeyPart(length: snapshot.data!.data.tags.length,tags: snapshot.data!.data.tags,),
                      const SizedBox(
                        height: 8,
                      ),
                      Padding(
                          padding: const EdgeInsets.only(right: 10),
                          child: Text(
                            "لیست های پخش پرطرفدار",
                            style: Theme.of(context).textTheme.headline6,
                          )),
                      const SizedBox(
                        height: 8,
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height / 5,
                        child: GridView.builder(
                            reverse: true,
                            scrollDirection: Axis.horizontal,
                            itemCount: snapshot.data!.data.topPlaylist.length,
                            gridDelegate:
                                const SliverGridDelegateWithMaxCrossAxisExtent(
                              maxCrossAxisExtent: 140,
                              childAspectRatio: 0.3,
                            ),
                            itemBuilder: (context, index) {
                              return topPlayList(
                                image: 'http://188.213.67.179:3200' +
                                    snapshot.data!.data.topPlaylist[index]
                                        .mobilePoster,
                                text:
                                    snapshot.data!.data.topPlaylist[index].name,
                                leftMargin: (index + 1 % 3 == 0) ? 10 : 0,
                                onTap: () {
                                  print(snapshot
                                      .data!.data.topPlaylist[index].name);
                                },
                              );
                            }),
                      ),
                      const SizedBox(
                        height: 8,
                      ),
                      Padding(
                          padding: const EdgeInsets.only(right: 10),
                          child: Text(
                            "موقعیتی",
                            style: Theme.of(context).textTheme.headline6,
                          )),
                      const SizedBox(
                        height: 8,
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height / 2.5,
                        child: ListView.builder(
                            reverse: true,
                            scrollDirection: Axis.horizontal,
                            itemCount:
                                snapshot.data!.data.locationPlaylist.length,
                            itemBuilder: (context, index) {
                              return locationPlayList(
                                image: 'http://188.213.67.179:3200' +
                                    snapshot.data!.data.locationPlaylist[index]
                                        .mobilePoster,
                                name: snapshot
                                    .data!.data.locationPlaylist[index].name,
                                leftMargin: (index ==
                                        snapshot.data!.data.locationPlaylist
                                                .length -
                                            1)
                                    ? 10
                                    : 0,
                                onTap: () {
                                  print(snapshot
                                      .data!.data.locationPlaylist[index].name);
                                },
                              );
                            }),
                      ),
                      const SizedBox(
                        height: 8,
                      ),
                      Padding(
                          padding: const EdgeInsets.only(left: 10, right: 10),
                          child: Stack(children: [
                            ClipRRect(
                                borderRadius: BorderRadius.circular(16),
                                child: SvgPicture.asset(
                                  'assets/image/Rectangle 198.svg',
                                  width: MediaQuery.of(context).size.width,
                                  height:
                                      MediaQuery.of(context).size.height / 3,
                                )),
                            Center(
                              child: Transform.rotate(
                                angle: 20 * 3.14 / 180,
                                child: Container(
                                  margin: const EdgeInsets.only(top: 100),
                                  width: 50,
                                  height: 50,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.zero,
                                      color: Colors.transparent,
                                      border: Border.all(color: Colors.white)),
                                      child: Text(
                                        "hi"
                                      ),
                                ),
                              ),
                            )
                          ])),
                      const SizedBox(
                        height: 8,
                      ),
                      Row(
                        children: [
                          viewAll(
                            onTap: () {
                              print('مشاهده همه');
                            },
                          ),
                          Padding(
                              padding: const EdgeInsets.only(right: 10),
                              child: Text(
                                "جدید ترین های پاپ",
                                style: Theme.of(context).textTheme.headline6,
                              )),
                        ],
                      ),
                      const SizedBox(
                        height: 8,
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height / 4,
                        child: ListView.builder(
                            reverse: true,
                            scrollDirection: Axis.horizontal,
                            itemCount: snapshot.data!.data.newestMusic.length,
                            itemBuilder: (context, index) {
                              return newestMusic(
                                image: 'http://188.213.67.179:3200' +
                                    snapshot.data!.data.newestMusic[index]
                                        .mobilePoster,
                                leftMargin: (index ==
                                        snapshot.data!.data.locationPlaylist
                                                .length -
                                            1)
                                    ? 10
                                    : 0,
                                nameFa: snapshot
                                    .data!.data.newestMusic[index].nameFa,
                                ArtistName: snapshot.data!.data
                                    .newestMusic[index].normalRbts[1].artist,
                                onTap: () {
                                  print(
                                      'snapshot.data!.data.newestMusic[index].normalRbts[1].artist');
                                },
                              );
                            }),
                      ),
                      const SizedBox(
                        height: 8,
                      ),
                      Padding(
                          padding: const EdgeInsets.only(right: 10),
                          child: Text(
                            'خواننده های یرطرفدار',
                            style: Theme.of(context).textTheme.headline6,
                          )),
                      const SizedBox(
                        height: 8,
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height / 2,
                        child: GridView.builder(
                            reverse: true,
                            scrollDirection: Axis.horizontal,
                            itemCount: snapshot.data!.data.bestArtists.length,
                            gridDelegate:
                                const SliverGridDelegateWithFixedCrossAxisCount(
                              // maxCrossAxisExtent: 200,
                              // mainAxisExtent: 150,
                              crossAxisSpacing: 12,
                              mainAxisSpacing: 12,
                              crossAxisCount: 2,
                              childAspectRatio: 1.65,
                            ),
                            itemBuilder: (context, index) {
                              return bestArtists(
                                image: 'http://188.213.67.179:3200' +
                                    snapshot.data!.data.bestArtists[index]
                                        .mobilePoster,
                                name: snapshot
                                    .data!.data.bestArtists[index].nameFa,
                                onTap: () {
                                  print('دنبال کردن');
                                },
                              );
                            }),
                      ),
                      const SizedBox(
                        height: 8,
                      ),
                      Padding(
                          padding: const EdgeInsets.only(right: 10),
                          child: Text(
                            'قومی محلی',
                            style: Theme.of(context).textTheme.headline6,
                          )),
                      const SizedBox(
                        height: 8,
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height / 2.5,
                        child: ListView.builder(
                            reverse: true,
                            scrollDirection: Axis.horizontal,
                            itemCount:
                                snapshot.data!.data.ethnicPlaylists.length,
                            itemBuilder: (context, index) {
                              return ethnicPlaylists(
                                image: 'http://188.213.67.179:3200' +
                                    snapshot.data!.data.ethnicPlaylists[index]
                                        .mobilePoster,
                                name: snapshot
                                    .data!.data.ethnicPlaylists[index].name,
                                leftMargin: (index ==
                                        snapshot.data!.data.locationPlaylist
                                                .length -
                                            1)
                                    ? 10
                                    : 0,
                                onTap: () {
                                  print('افزودن به لیست ها');
                                },
                              );
                            }),
                      ),
                      const SizedBox(
                        height: 8,
                      ),
                      Row(
                        children: [
                          viewAll(
                            onTap: () {
                              print('مشاهده همه');
                            },
                          ),
                          Padding(
                              padding: const EdgeInsets.only(right: 10),
                              child: Text(
                                "داغ ترین ها",
                                style: Theme.of(context).textTheme.headline6,
                              )),
                        ],
                      ),
                      const SizedBox(
                        height: 8,
                      ),
                      Padding(
                        padding: EdgeInsets.all(10),
                        child: SizedBox(
                          width: MediaQuery.of(context).size.width,
                          height: MediaQuery.of(context).size.height / 3,
                          child: Row(
                            children: [
                              ClipRRect(
                                  borderRadius: BorderRadius.circular(8),
                                  child: Image.asset(
                                    'assets/image/G-EAZY.jpg',
                                    width:
                                        MediaQuery.of(context).size.width / 2.2,
                                    height:
                                        MediaQuery.of(context).size.height / 3,
                                  )),
                              const SizedBox(
                                width: 8,
                              ),
                              Column(
                                children: [
                                  ClipRRect(
                                      borderRadius: BorderRadius.circular(8),
                                      child: Image.asset(
                                          'assets/image/images (1).png',
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width /
                                              2.2,
                                          height: MediaQuery.of(context)
                                                  .size
                                                  .height /
                                              6.2,
                                          fit: BoxFit.fill)),
                                  const SizedBox(
                                    height: 8,
                                  ),
                                  ClipRRect(
                                      borderRadius: BorderRadius.circular(8),
                                      child: Image.asset(
                                        'assets/image/avatars.jpg',
                                        width:
                                            MediaQuery.of(context).size.width /
                                                2.2,
                                        height:
                                            MediaQuery.of(context).size.height /
                                                6.2,
                                        fit: BoxFit.fill,
                                      )),
                                ],
                              )
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ));
              } else {
                return const Center(
                  child: CircularProgressIndicator(),
                );
              }
            }));
  }
}


class viewAll extends StatelessWidget {

  final Function() onTap;
  const viewAll({
    Key? key,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Padding(
          padding: const EdgeInsets.only(left: 10),
          child: InkWell(
              onTap: onTap,
              child: Text(
                "مشاهده همه",
                style: Theme.of(context).textTheme.headline5,
              ))),
    );
  }
}
