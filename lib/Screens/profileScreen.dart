import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class profileScreen extends StatefulWidget {
  const profileScreen({super.key});

  @override
  State<profileScreen> createState() => _profileScreenState();
}

class _profileScreenState extends State<profileScreen> {
  bool test = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SizedBox(
          width: double.infinity,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: [
              AnimatedContainer(
                height: 100,
                width: 100,
                duration: Duration(milliseconds: 500),
                padding: EdgeInsets.symmetric(horizontal: 8),
                decoration: BoxDecoration(
                  color: test ? Colors.red : Colors.blue,
                ),
                alignment:test? Alignment.centerRight : Alignment.centerLeft,
                child: Text(
                  test.toString(),
                ),
              ),
              const SizedBox(height: 100),
              ElevatedButton(
                  onPressed: () {
                    setState(() {
                      test = !test;
                    });
                  },
                  child: Text("changer"))
            ],
          ),
        ),
      ),
    );
  }
}
