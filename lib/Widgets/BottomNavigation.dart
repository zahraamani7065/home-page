import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:home_page_project/main.dart';

class bottomNavigation extends StatefulWidget {
  final Function(int index) onTap;
  final int selectedIndex;

  const bottomNavigation(
      {super.key, required this.onTap, required this.selectedIndex});

  @override
  State<bottomNavigation> createState() => _bottomNavigationState();
}

class _bottomNavigationState extends State<bottomNavigation> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      decoration: BoxDecoration(
        color: Colors.grey.shade700,
      ),
      child: Padding(
        padding: EdgeInsets.only(left: 30, right: 30),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            BottomNavigationItem(
              iconFileName: Icons.home,
              activeIconFileName: 'home',
              title: 'home',
              onTap: () {
                widget.onTap(homeIndex);
              },
              isActive: widget.selectedIndex == homeIndex,
            ),

            BottomNavigationItem(
              iconFileName: Icons.play_circle,
              activeIconFileName: 'play',
              title: 'play',
              onTap: () {
                widget.onTap(playIndex);
              },
              isActive: widget.selectedIndex == playIndex,
            ),
            BottomNavigationItem(
              iconFileName: Icons.search,
              activeIconFileName: 'search',
              title: 'search',
              onTap: () {
                widget.onTap(searchIndex);
              },
              isActive: widget.selectedIndex == searchIndex,
            ),
            BottomNavigationItem(
              iconFileName: Icons.call,
              activeIconFileName: 'call',
              title: 'call',
              onTap: () {
                widget.onTap(callIndex);
              },
              isActive: widget.selectedIndex == callIndex,
            ),

            BottomNavigationItem(
              iconFileName: Icons.person,
              activeIconFileName: 'person',
              title: 'person',
              onTap: () {
                widget.onTap(profileIndex);
              },
              isActive: widget.selectedIndex == profileIndex,
            ),
          ],
        ),
      ),
    );
  }
}

class BottomNavigationItem extends StatefulWidget {
  final IconData iconFileName;
  final String activeIconFileName;
  final String title;
  final bool isActive;
  final Function() onTap;

  const BottomNavigationItem(
      {super.key,
      required this.iconFileName,
      required this.activeIconFileName,
      required this.title,
      required this.onTap,
      required this.isActive});

  @override
  State<BottomNavigationItem> createState() => _BottomNavigationItemState();
}

class _BottomNavigationItemState extends State<BottomNavigationItem> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: widget.onTap,
        child:
        AnimatedContainer(
            duration:const Duration(seconds: 3),
            decoration:  BoxDecoration(
              borderRadius: BorderRadius.circular(16),
              color: Colors.grey.shade700,
            ),
            curve: Curves.fastOutSlowIn,
            child: widget.isActive ? Container(
              decoration:  BoxDecoration(
                borderRadius: BorderRadius.circular(16),
                color: Colors.grey.shade600,
              ),
              child: Row(
                children: [
                  Icon(widget.iconFileName),
                  const SizedBox(
                    width: 2,
                  ),
                  Text(
                    widget.title,
                    style: const TextStyle(color: Colors.white38),
                  ),
                  const SizedBox(
                    width: 4,
                  )
                ],
              ),)
                : SizedBox(
                  child: Icon(
                  widget.iconFileName,
                  color: Colors.black54,
              ),
                ),


        )

    );

  }
}
