import 'package:flutter/material.dart';

class bestArtists extends StatelessWidget{
  final String image;
  final String name;
  final Function() onTap;

  const bestArtists({super.key, required this.image, required this.name, required this.onTap});
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(0),
      child: Column(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(24), child: Image.network(image,fit: BoxFit.cover,height: 100, ),),
          SizedBox(height: 4,),
          Text(name),
          SizedBox(height: 4,),
          InkWell(
            onTap: onTap,
              child: Container(padding: EdgeInsets.all(7), decoration: BoxDecoration(borderRadius: BorderRadius.circular(16),color: Colors.black87),child: Text('دنبال کردن',style: TextStyle(color: Colors.white70)),))

        ],


      ),
    );
  }


}