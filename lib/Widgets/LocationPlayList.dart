import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class locationPlayList extends StatelessWidget{
  final double leftMargin;
  final Function() onTap;
  final String image;
  final String name;

  const locationPlayList({super.key, required this.image, required this.name, required this.leftMargin, required this.onTap, });
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        margin: EdgeInsets.fromLTRB(leftMargin, 10, 13,0),
        width: MediaQuery.of(context).size.width/1.6,
        child: Stack(children: [
          Positioned.fill(child: ClipRRect(borderRadius: BorderRadius.circular(8),
              child:ImageFiltered(imageFilter:  ImageFilter.blur(sigmaX: 20, sigmaY: 20),
                child: Image.network(image,fit: BoxFit.fill ),) )),
          Padding( padding: EdgeInsets.fromLTRB(30, 20, 30, 15),
            child: Column(children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(8),
                child: Image.network(image,fit: BoxFit.fill,width: MediaQuery.of(context).size.width/2,height: MediaQuery.of(context).size.height/5,),),
              const SizedBox(height: 20,),
              Text(name),
              const SizedBox(height: 30,),
              Container(height: 30,
                padding: EdgeInsets.only(left: 10,right: 10,top: 3),
                decoration: BoxDecoration(color: Colors.yellow.shade700,borderRadius: BorderRadius.circular(16)),child: Text('افزودن به لیستها'),)

            ],),
          )
        ], ),

      ),
    );
  }

}