import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class newestMusic extends StatelessWidget{
  final String image;
  final String nameFa;
  final String ArtistName;
  final double leftMargin;
  final Function() onTap;

  const newestMusic({super.key, required this.image, required this.nameFa, required this.ArtistName, required this.leftMargin, required this.onTap});
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        margin: EdgeInsets.only(right: 10,left: leftMargin),
        child: Column(
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(8),
              child: Image.network(image,fit: BoxFit.fill,width: MediaQuery.of(context).size.width/4,height: MediaQuery.of(context).size.height/7,),),

            const SizedBox(height: 10,),
            Text(nameFa),
            const SizedBox(height: 10,),
            Text(ArtistName)
          ],


        ),
      ),
    );
  }

}