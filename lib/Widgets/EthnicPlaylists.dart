import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ethnicPlaylists extends StatelessWidget{
  final String image;
  final double leftMargin;
  final String name;
  final Function() onTap;

  const ethnicPlaylists({super.key, required this.image, required this.leftMargin, required this.name, required this.onTap});
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(leftMargin, 10, 13,0),
      width: MediaQuery.of(context).size.width/1.6,
      child: Stack(children: [
        Positioned.fill(child: ClipRRect(borderRadius: BorderRadius.circular(8),
            child:ImageFiltered(imageFilter:  ImageFilter.blur(sigmaX: 20, sigmaY: 20),
              child: Image.network(image,fit: BoxFit.fill ),) )),
        Padding( padding: const EdgeInsets.fromLTRB(30, 20, 30, 15),
          child: Column(children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(8),
              child: Image.network(image,fit: BoxFit.fill,width: MediaQuery.of(context).size.width/2,height: MediaQuery.of(context).size.height/5,),),
            const SizedBox(height: 20,),
            Text(name),
            const SizedBox(height: 30,),
            InkWell(
              onTap: onTap,
              child: Container(height: 30,
                padding: const EdgeInsets.only(left: 10,right: 10,top: 3),
                decoration: BoxDecoration(color: Colors.yellow.shade700,borderRadius: BorderRadius.circular(16)),child: Text('افزودن به لیستها'),),
            )

          ],),
        )
      ], ),

    );
  }

}