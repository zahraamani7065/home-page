import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class topPlayListPart extends StatelessWidget {
  const topPlayListPart({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.only(right: 10),
        child: Text(
          "لیست های پخش پرطرفدار",
          style: Theme.of(context).textTheme.headline6,
        ));
  }
}