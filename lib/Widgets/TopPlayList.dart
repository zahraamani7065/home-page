import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class topPlayList extends StatelessWidget {
  final String image;
  final String text;
  final double leftMargin;
  final Function() onTap;
  const topPlayList({
    Key? key, required this.image, required this.text, required this.leftMargin, required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return
      InkWell(
        onTap: onTap,
        child: Container(
          margin: EdgeInsets.only(left: leftMargin,right: 10,top: 10),
          child: Stack(
            children: [

              Positioned.fill(child: ClipRRect(borderRadius: BorderRadius.circular(8),
                  child:ImageFiltered(imageFilter:  ImageFilter.blur(sigmaX: 10, sigmaY: 10),
                    child: Image.network(image,fit: BoxFit.cover),) )),

              Padding( padding:const EdgeInsets.all(10) ,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end
                  ,children: [
                  Text(text,style: Theme.of(context).textTheme.subtitle1,),
                  const SizedBox(width: 8,),
                  ClipRRect(borderRadius: BorderRadius.circular(8),
                    child: Image.network(image,fit: BoxFit.cover,), ),
                ],
                ),
              ),
            ],),
        ),
      );
  }
}
